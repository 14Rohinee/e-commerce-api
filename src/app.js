import express from 'express';
import 'dotenv/config';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import { API_VERSION } from './config/app.js';
import appRoutes from './routes/back-end/index.js';
const app = express();

app.use(bodyParser.json());

app.use(API_VERSION, appRoutes);

// Check for MongoDB connection
// eslint-disable-next-line no-undef
const dbConnection = mongoose.connect(process.env.DATABASE_URL).then(() => {
	// eslint-disable-next-line no-undef
	app.listen(process.env.PORT, () => {
		// eslint-disable-next-line no-undef
		console.log(`server is listening on port: ${process.env.PORT}`);
	});
}).catch((error) => {
	console.error('Error connecting to MongoDB:', error.message);
});

export default dbConnection;