import Company from '../models/companyModel.js';
import Tax from '../models/taxModel.js';
import seeder from './seeder.js';

const taxSeeder = async () => {
	try {
		const company = await Company.findOne();
		const taxes = [
			{
				companyId: company._id,
				name: 'GST',
				rate: 18,
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'VAT',
				rate: 10,
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'CGST',
				rate: 9,
				status: 'active',
				createdAt: new Date()
			}
		];
	
		await seeder(Tax, taxes, 'Tax');
	} catch (error) {
		console.log('Enable to get company in tax seeder:', error);
	}
};
export default taxSeeder;