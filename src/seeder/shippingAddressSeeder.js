import ShippingAddress from '../models/shippingAddressModel.js';
import seeder from './seeder.js';
import User from '../models/userModel.js';

const shippingAddressSeeder = async () => {
	try {
		const users = await User.find();
		const shippingAddresses = [
			{
				userId: users[0]._id,
				name: users[0].name,
				phone: users[0].mobile,
				pincode: '123456',
				address: '123 Street',
				landmark: 'Thaggu ke samose',
				houseNumber: '123/456',
				city: 'Kochi',
				state: 'Kerala',
				country: 'India',
				status: 'active',
				zip: '123456',
				isDefaultAddress: true,
				createdAt: new Date()
			},
			{
				userId: users[1]._id,
				name: users[1].name,
				phone: users[1].mobile,
				pincode: '984657',
				address: '087 winston street',
				landmark: 'Henoi tower',
				houseNumber: '87/989',
				city: 'Houston',
				state: 'Texas',
				country: 'United State',
				status: 'active',
				zip: '30098',
				isDefaultAddress: false,
				createdAt: new Date()
			},
		];
	
		await seeder(ShippingAddress, shippingAddresses, 'ShippingAddress');
	} catch (error) {
		console.log('Unable to get users in shipping address seeder:', error);
	}
};

export default shippingAddressSeeder ;