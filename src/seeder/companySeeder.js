import Company from '../models/companyModel.js';
import seeder from './seeder.js';

const companies = {
	name: 'Example Company',
	email: 'company1@example.com',
	phone: 1234567890,
	website: 'company1.com',
	dateFormat: 'YYYY-MM-DD',
	timeFormat: 'HH:mm:ss',
	timeZone: 'Asia/Kolkata',
	appName: 'E-COMMERCE',
	createdAt: new Date()
};

const companySeeder = async () => {
	try {
		await seeder(Company, companies, 'Company');
	} catch (error) {
		console.log('Error Occurred in companySeeder: ', error);
		throw error;
	}
};


export default companySeeder;