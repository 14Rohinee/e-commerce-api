import Language from '../models/languageModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const languageSeeder = async () => {
	try {
		const company = await Company.findOne();
		
		const languages = [
			{
				companyId: company._id,
				name: 'English',
				code: 'en',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Hindi',
				code: 'hi',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'French',
				code: 'fr',
				status: 'active',
				createdAt: new Date()
			}
		];
	
		await seeder(Language, languages, 'Language');
	} catch (error) {
		console.log('Enable to get company in coupon seeder:', error);
	}
};

export default languageSeeder;