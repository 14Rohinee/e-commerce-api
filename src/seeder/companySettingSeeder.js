import CompanySetting from '../models/companySettingModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const companySettingSeeder = async () => {

	try {
		const company = await Company.findOne();
		
		const companySettings = {
			companyId: company._id,
			paypalStatus: false,
			paypalEnvironment: 'sandbox',
			paypalSandboxClientId: '',
			paypalSandboxClientSecret: '',
			paypalLiveClientId: '',
			paypalLiveClientSecret: '',
			stripeStatus: false,
			stripeEnvironment: 'sandbox',
			stripeSandboxPublishableKey: '',
			stripeSandboxSecretKey: '',
			stripeLivePublishableKey: '',
			stripeLiveSecretKey: '',
			razorpayStatus: false,
			razorpayEnvironment: 'sandbox',
			razorpaySandboxKeyId: '',
			razorpaySandboxKeySecret: '',
			razorpayLiveKeyId: '',
			razorpayLiveKeySecret: ''
		};
	
		await seeder(CompanySetting, companySettings, 'CompanySetting');
	} catch (error) {
		console.log('Enable to get company in coupon seeder:', error);
	}
};

export default companySettingSeeder;