import Company from '../models/companyModel.js';
import SubCategory from '../models/subCategoryModel.js';
import seeder from './seeder.js';
import Category from '../models/categoryModel.js';

const subCategorySeeder = async () => {
	try {
		const company = await Company.findOne();
		const categories = await Category.find();
	
		const subcategories = [];
		for (const category of categories) {
			for (let i = 1; i <= 5; i++) {
				subcategories.push({
					companyId: company._id,
					categoryId: category._id,
					name: `${category.name} ${i}`,
					slug: `${category.slug}-${i}`,
					status: 'active',
					createdAt: new Date()
				});
			}
		}
	
		await seeder(SubCategory, subcategories, 'SubCategory');
	} catch (error) {
		console.log('Error Occur while seeding sub category:', error);
	}
};

export default subCategorySeeder;