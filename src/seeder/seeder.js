const seeder = async (Model, variable, ModelName) => {
	try {
		const existingData = await Model.find();

		if (existingData) {
			await Model.deleteMany({});
		}

		await Model.insertMany(variable);
		console.log(`${ModelName} Seeded Successfully`);
	} catch (error) {
		console.log(`Error while seeding ${ModelName}:`, error);

		// eslint-disable-next-line no-undef
		process.exit(1);
	}
};

export default seeder;
