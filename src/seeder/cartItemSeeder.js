import CartItem from '../models/cartItemModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';
import Product from '../models/productModel.js';
import Cart from '../models/cartModel.js';

const cartItemSeeder = async () => {
	try {
		const products = await Product.find();
		const carts = await Cart.find();
		const company = await Company.findOne();
		const cartItems = [];

		for (const cart of carts) {
			for (const product of products) {
				cartItems.push({
					companyId: company._id,
					cartId: cart._id,
					productId: product._id,
					quantity: 1,
					unitPrice: product.price,
					totalPrice: product.price
				});
			}
		}
		await seeder(CartItem, cartItems, 'CartItem');
	} catch (error) {
		console.log('Enable to get company or products or carts in cart item seeder:', error);
	}
};

export default cartItemSeeder;