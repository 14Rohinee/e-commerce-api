import Company from '../models/companyModel.js';
import seeder from './seeder.js';
import SmsNotificationSetting from '../models/smsNotificationSettingModel.js';

const smsNotificationSettingSeeder = async() => {

	try {
		const company = await Company.findOne();
		const smsNotificationSettings = {
			companyId: company._id,
			smsFromNumber: '1234567890',
			smsSid: 'rohineetandekar',
			smsAuthToken: '1232wsdfsda',
			status: 'active'
		};
	
		await seeder(SmsNotificationSetting, smsNotificationSettings, 'SmsNotificationSetting');
	} catch (error) {
		console.log('Enable to get company in smsNotification setting seeder:', error);
	}
};

export default smsNotificationSettingSeeder;