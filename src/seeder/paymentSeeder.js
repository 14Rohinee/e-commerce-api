import Company from '../models/companyModel.js';
import seeder from './seeder.js';
import Order from '../models/orderItemModel.js';
import User from '../models/userModel.js';
import Payment from '../models/paymentModel.js';

const paymentSeeder = async () => {
	try {
		const users = await User.find();
		const orders = await Order.find();
		const company = await Company.findOne();
		const payments = [];

		for (const order of orders) {
			payments.push({
				companyId: company._id,
				orderId: order._id,
				paymentMethod: 'razorpay',
				status: 'Completed',
				amount: 1000,
				createdBy: users[1]._id,
				createdAt: new Date()
			});
		}

		await seeder(Payment, payments, 'Payment');
	} catch (error) {
		console.log('Enable to get company or orders in payment seeder:', error);
	}
};

export default paymentSeeder;