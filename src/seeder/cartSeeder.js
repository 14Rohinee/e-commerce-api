import Cart from '../models/cartModel.js';
import Company from '../models/companyModel.js';
import seeder from './seeder.js';
import User from '../models/userModel.js';
import ShippingAddress from '../models/shippingAddressModel.js';

const cartSeeder = async () => {
	try {
		const company = await Company.findOne();
		const users = await User.find();
		const shippingAddresses = await ShippingAddress.find();

		const carts = [
			{
				companyId: company._id,
				userId: users[0]._id,
				addressId: shippingAddresses[0]._id,
				subTotal: 1000,
				discount: 0,
				tax: 0,
				deliveryFee: 10,
				total: 1010,
				deliveryInstruction: 'Please deliver ASAP',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[0]._id,
				addressId: shippingAddresses[1]._id,
				subTotal: 2000,
				discount: 0,
				tax: 0,
				deliveryFee: 20,
				total: 2020,
				deliveryInstruction: 'Please deliver ASAP',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[0]._id,
				addressId: shippingAddresses[1]._id,
				subTotal: 3000,
				discount: 0,
				tax: 0,
				deliveryFee: 30,
				total: 3030,
				deliveryInstruction: 'Please deliver ASAP',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[1]._id,
				addressId: shippingAddresses[0]._id,
				subTotal: 4000,
				discount: 0,
				tax: 0,
				deliveryFee: 40,
				total: 4040,
				deliveryInstruction: 'Please deliver ASAP',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[1]._id,
				addressId: shippingAddresses[1]._id,
				subTotal: 5000,
				discount: 0,
				tax: 0,
				deliveryFee: 50,
				total: 5050,
				deliveryInstruction: 'Please deliver ASAP',
				createdAt: new Date()
			}
		];
		
		await seeder(Cart, carts, 'Cart');
	} catch (error) {
		console.log('Enable to get company or users or shipping addresses in cart seeder:', error);
	}
};

export default cartSeeder;