import Product from '../models/productModel.js';
import Company from '../models/companyModel.js';
import seeder from './seeder.js';
import Brand from '../models/brandModel.js';
import Category from '../models/categoryModel.js';

const productSeeder = async () => {
	try {
		const categories = await Category.find();
		const brands = await Brand.find();
		const company = await Company.findOne();

		const products = [
			{
				companyId: company._id,
				name: 'Sony Vaio',
				slug: 'sony-vaio',
				shortDescription: 'The Sony Vaio is a line of stylish and high-performance laptops known for their sleek design, powerful hardware, and cutting-edge features. With advanced technology and premium build quality, Sony Vaio laptops offer users a seamless computing experience for both work and entertainment.',
				longDescription: 'The Sony Vaio is a renowned series of laptops that exemplify innovation and quality in the realm of portable computing. Designed with meticulous attention to detail, these laptops blend cutting-edge technology with stylish aesthetics to cater to the diverse needs of users. Featuring robust hardware configurations, vibrant displays, and ergonomic keyboards, Sony Vaio laptops deliver exceptional performance and user comfort. From powerful processors to ample storage options and advanced graphics capabilities, these laptops are adept at handling demanding tasks ranging from multimedia editing to gaming and productivity applications. Moreover, the integration of intuitive software enhancements and connectivity options ensures seamless integration into modern digital lifestyles. Whether you are a professional seeking productivity tools, a creative individual pursuing artistic endeavors, or a casual user in search of immersive entertainment experiences, the Sony Vaio line offers a versatile range of options to suit your preferences and elevate your computing experience to new heights',
				brandId: brands[0]._id,
				categoryId: categories[0]._id,
				price: 1000,
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: '	',
				slug: 'samsung-galaxy',
				shortDescription: 'The Samsung Galaxy is a popular line of smartphones known for its sleek design, cutting-edge features, and powerful performance. Offering a diverse range of models to suit various needs and budgets, Samsung Galaxy devices combine premium build quality with innovative technology to deliver an exceptional mobile experience',
				longDescription: 'The Samsung Galaxy represents a diverse and comprehensive range of smartphones that epitomize excellence in the mobile industry. Renowned for their sleek design, cutting-edge features, and robust performance, Samsung Galaxy devices cater to the diverse needs and preferences of users worldwide. Boasting stunning displays, advanced camera systems, and powerful processors, each Galaxy model offers a compelling blend of innovation and practicality. From flagship models featuring state-of-the-art technology to more affordable options that prioritize accessibility without compromising on quality, the Samsung Galaxy lineup ensures that there is a device tailored to every user requirements. With an emphasis on seamless integration, intuitive user interfaces, and a wide array of software features, Galaxy smartphones empower users to accomplish more, stay connected, and express themselves creatively in today digital landscape. Whether you are capturing unforgettable moments, staying productive on the go, or enjoying immersive multimedia experiences, the Samsung Galaxy series sets the standard for excellence in mobile technology, continually pushing the boundaries of innovation to redefine the possibilities of what a smartphone can achieve',
				brandId: brands[1]._id,
				categoryId: categories[1]._id,
				price: 2000,
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Apple Watch',
				slug: 'apple-watch',
				shortDescription: 'The Apple Watch is a sophisticated wearable device designed by Apple Inc., offering a seamless blend of style and functionality. With its sleek design, advanced health and fitness tracking features, and seamless integration with other Apple devices, the Apple Watch enhances convenience and productivity while keeping users connected and informed on the go',
				longDescription: 'The Apple Watch stands as a pinnacle of wearable technology, meticulously crafted by Apple Inc. to redefine the way users interact with their digital lives. Combining elegant aesthetics with cutting-edge innovation, the Apple Watch serves as more than just a timepiece, but a versatile companion that seamlessly integrates into daily routines. Featuring a sleek and customizable design, the Apple Watch offers users the opportunity to express their personal style while enjoying a wealth of features and capabilities. From advanced health and fitness tracking functionalities, including heart rate monitoring, activity tracking, and workout metrics, to convenient communication tools such as calls, messages, and notifications, the Apple Watch empowers users to stay connected, informed, and motivated throughout their day. Furthermore, with its seamless integration with other Apple devices and services, including iPhone, iPad, and Apple Health, the Apple Watch provides a holistic ecosystem that enhances productivity, convenience, and overall well-being. Whether it is monitoring health goals, managing daily schedules, or accessing a wide range of apps and services from the wrist, the Apple Watch represents a paradigm shift in wearable technology, offering users unparalleled versatility, functionality, and style in a compact and elegant form factor',
				brandId: brands[2]._id,
				categoryId: categories[2]._id,
				price: 3000,
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Sony Vaio 2',
				slug: 'sony-vaio-2',
				shortDescription: 'Sony Vaio 2 is the latest iteration of Sony renowned line of laptops, characterized by its sleek design, robust performance, and innovative features. With enhanced hardware configurations and intuitive software enhancements, Sony Vaio 2 delivers a seamless computing experience for users seeking productivity, creativity, and entertainment',
				longDescription: 'Sony Vaio 2 represents the pinnacle of Sony legacy in laptop innovation, embodying the perfect balance of style, performance, and versatility. Building upon the success of its predecessors, the Sony Vaio 2 boasts a refined design language, premium materials, and meticulous attention to detail that set it apart in the competitive landscape of portable computing. Equipped with cutting-edge hardware configurations, including powerful processors, vibrant displays, and ample storage options, the Sony Vaio 2 offers unmatched performance across a wide range of tasks, from intensive productivity applications to multimedia editing and gaming. Moreover, the integration of intuitive software enhancements and connectivity features ensures seamless interaction with the digital world, allowing users to stay productive and connected wherever they go. Whether it is the sleek and lightweight design that makes it ideal for professionals on the move or the immersive multimedia experience that caters to entertainment enthusiasts, the Sony Vaio 2 is designed to exceed expectations and elevate the computing experience to new heights. With its blend of style, performance, and innovation, Sony Vaio 2 remains a compelling choice for users seeking a premium laptop that delivers on both form and function',
				brandId: brands[0]._id,
				categoryId: categories[0]._id,
				price: 4000,
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Samsung Galaxy 2',
				slug: 'samsung-galaxy-2',
				shortDescription: 'Samsung Galaxy 2 is the latest iteration of Samsung is flagship smartphone line, renowned for its sleek design, advanced features, and powerful performance. With cutting-edge technology, vibrant displays, and enhanced camera systems, Samsung Galaxy 2 offers users an unparalleled mobile experience tailored to their diverse needs',
				longDescription: 'Samsung Galaxy 2 represents the epitome of innovation and excellence in the realm of smartphones, building upon the success of its predecessors to deliver a truly immersive and transformative mobile experience. With its sleek and stylish design, Samsung Galaxy 2 captivates users from the moment they lay eyes on its premium craftsmanship and attention to detail. Beyond its aesthetics, the Samsung Galaxy 2 sets itself apart with a host of advanced features and capabilities designed to meet the demands of modern lifestyles. From vibrant displays that bring content to life with stunning clarity and detail to powerful processors that effortlessly handle multitasking and demanding applications, the Samsung Galaxy 2 is engineered to keep pace with the needs of today users. Furthermore, the device is enhanced camera systems, including state-of-the-art sensors and intelligent software algorithms, empower users to capture moments with unparalleled clarity, detail, and creativity. Whether it is sharing memories with loved ones, staying productive on the go, or immersing oneself in multimedia content, the Samsung Galaxy 2 offers a seamless and intuitive user experience that redefines the possibilities of mobile technology. With its seamless integration of innovative features, robust performance, and sleek design, Samsung Galaxy 2 continues to set the standard for excellence in the smartphone industry, inspiring and empowering users to explore, create, and connect in new and exciting ways',
				brandId: brands[1]._id,
				categoryId: categories[1]._id,
				price: 5000,
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				status: 'active',
				createdAt: new Date()
			}
		];
		await seeder(Product, products, 'Product');
	} catch (error) {
		console.log('Enable to get company or categories or brand in product seeder:', error);
	}
};

export default productSeeder;