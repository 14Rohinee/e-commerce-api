import EmailSetting from '../models/emailSettingModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const emailSettingSeeder = async () => {

	try {
		const company = await Company.findOne();
		const emailSettings = {
			companyId: company._id,
			mailFromName: 'Rohinee Tandekar',
			mailFromAddress: 'rohinee@example.com',
			driver: 'smtp',
			host: '45445',
			port: '2000',
			username: 'rohineetandekar',
			password: '123456',
			encryption: '1234',
			status: 'inactive',
			createdAt: new Date()
		};
	
		await seeder(EmailSetting, emailSettings, 'EmailSetting');
	} catch (error) {
		console.log('Enable to get company in coupon seeder:', error);
	}
};

export default emailSettingSeeder;