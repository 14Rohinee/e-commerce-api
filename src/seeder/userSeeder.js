import User from '../models/userModel.js';
import bcrypt from 'bcrypt';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const userSeeder = async () => {
	try {
		let company = await Company.findOne();
		const users = [
			{
				companyId: company._id,
				name: 'John Doe',
				email: 'admin@example.com',
				password: bcrypt.hashSync('1234567890', 10),
				mobile: '1234567890',
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				role: 'admin',
				status: 'active',
				verifiedAt: new Date(),
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Jane Doe',
				email: 'client@example.com',
				password: bcrypt.hashSync('123456', 10),
				mobile: '1234567891',
				image: 'https://live.staticflickr.com/7631/26849088292_36fc52ee90_b.jpg',
				role: 'customer',
				status: 'active',
				verifiedAt: new Date(),
				createdAt: new Date()
			}
		];

		await seeder(User, users, 'User');
	} catch (error) {
		console.log('Error Occurred in userSeeder: ', error);
	}
};

export default userSeeder;