import Coupon from '../models/couponModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const couponSeeder = async () => {

	try {
		const company = await Company.findOne();
		const coupons = [
			{
				companyId: company._id,
				code: 'OFF10',
				discountType: 'fixed',
				discount: 10,
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				code: 'OFF20',
				discountType: 'fixed',
				discount: 20,
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				code: 'OFF30',
				discountType: 'fixed',
				discount: 30,
				status: 'active',
				createdAt: new Date()
			}
		];
	
		await seeder(Coupon, coupons, 'Coupon');
	} catch (error) {
		console.log('Enable to get company in coupon seeder:', error);
	}
};

export default couponSeeder;