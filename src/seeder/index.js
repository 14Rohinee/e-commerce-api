import dbConnection from '../app.js';
import companySeeder from './companySeeder.js';
import userSeeder from './userSeeder.js';
import shippingAddressSeeder  from './shippingAddressSeeder.js';
import brandSeeder from './brandSeeder.js';
import categorySeeder  from './categorySeeder.js';
import subCategorySeeder from './subCategorySeeder.js';
import cartSeeder from './cartSeeder.js';
import cartItemSeeder from './cartItemSeeder.js';
import productSeeder from './productSeeder.js';
import orderSeeder from './orderSeeder.js';
import orderItemSeeder from './orderItemSeeder.js';
import couponSeeder from './couponSeeder.js';
import companySettingSeeder from './companySettingSeeder.js';
import emailSettingSeeder from './emailSettingSeeder.js';
import languageSeeder from './languageSeeder.js';
import paymentSeeder from './paymentSeeder.js';
import pushNotificationSettingSeeder from './pushNotificationSettingSeeder.js';
import smsNotificationSettingSeeder from './smsNotificationSettingSeeder.js';
import socialAuthSettingSeeder from './socialAuthSettingSeeder.js';
import taxSeeder from './taxSeeder.js';

const seedData = async () => {
	try {
		if (dbConnection) {
			await companySeeder();
			await userSeeder();
			await shippingAddressSeeder();
			await brandSeeder();
			await categorySeeder();
			await subCategorySeeder();
			await cartSeeder();
			await cartItemSeeder();
			await productSeeder();
			await orderSeeder();
			await orderItemSeeder();
			await couponSeeder();
			await companySettingSeeder();
			await emailSettingSeeder();
			await languageSeeder();
			await paymentSeeder();
			await pushNotificationSettingSeeder();
			await smsNotificationSettingSeeder();
			await socialAuthSettingSeeder();
			await taxSeeder();

		} else {
			console.log('Error while connecting to database');
		}
	} catch (error) {
		console.log('Error Occurred: ', error);
	}
};

seedData();