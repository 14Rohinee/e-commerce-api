import Category from '../models/categoryModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const categorySeeder = async () => {
	try {
		const company = await Company.findOne();
		const categories = [
			{
				companyId: company._id,
				name: 'Laptop',
				slug: 'laptop',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Mobile',
				slug: 'mobile',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Watch',
				slug: 'watch',
				status: 'active',
				createdAt: new Date()
			}
		];
	
		await seeder(Category, categories, 'Category');
	} catch (error) {
		console.log('Enable to get company in category seeder:', error);
	}
};
export default categorySeeder;