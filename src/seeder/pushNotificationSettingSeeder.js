import PushNotificationSetting from '../models/pushNotificationSettingModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const pushNotificationSettingSeeder = async () => {

	try {
		const company = await Company.findOne();
		const pushNotificationSettings = {
			companyId: company._id,
			onesignalAppId: '234sdfdas',
			onesignalApiKey: 'sdfsdg34423',
			status: 'inactive',
			createdAt: new Date()
		};
	
		await seeder(PushNotificationSetting, pushNotificationSettings, 'PushNotificationSetting');
	} catch (error) {
		console.log('Enable to get company in push notification setting seeder:', error);
	}
};

export default pushNotificationSettingSeeder;