import Company from '../models/companyModel.js';
import seeder from './seeder.js';
import SocialAuthSetting from '../models/socialAuthSettingModel.js';

const socialAuthSettingSeeder = async () => {

	try {
		const company = await Company.findOne();
		const socialAuthSettings = {
			companyId: company._id,
			facebookClientId: '',
			facebookClientSecret: '',
			facebookStatus: 'inactive',
			googleClientId: '',
			googleClientSecret: '',
			googleStatus: 'inactive',
			linkedinClientId: '',
			linkedinClientSecret: '',
			linkedinStatus: 'inactive',
			twitterClientId: '',
			twitterClientSecret: '',
			twitterStatus: 'inactive',
			createdAt: new Date()
		};
	
		await seeder(SocialAuthSetting, socialAuthSettings, 'SocialAuthSetting');
	} catch (error) {
		console.log('Enable to get company in social auth setting seeder:', error);
	}
};

export default socialAuthSettingSeeder;