import OrderItem from '../models/orderItemModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';
import Order from '../models/orderItemModel.js';
import Product from '../models/productModel.js';

const orderItemSeeder = async () => {
	try {
		const products = await Product.find();
		const orders = await Order.find();
		const company = await Company.findOne();
		const orderItems = [];

		for (const order of orders) {
			for (const product of products) {
				orderItems.push({
					companyId: company._id,
					orderId: order._id,
					productId: product._id,
					quantity: 1,
					price: product.price,
					total: product.price
				});
			}
		}

		await seeder(OrderItem, orderItems, 'OrderItem');
		
	} catch (error) {
		console.log('Enable to get company or products or orders in order item seeder:', error);
	}
};

export default orderItemSeeder;