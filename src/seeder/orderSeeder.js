import Order from '../models/orderModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';
import ShippingAddress from '../models/shippingAddressModel.js';
import User from '../models/userModel.js';

const orderSeeder = async () => {
	try {
		const shippingAddresses = await ShippingAddress.find();
		const users = await User.find();
		const company = await Company.findOne();

		const orders = [
			{
				companyId: company._id,
				userId: users[0]._id,
				paymentId: shippingAddresses[1]._id,
				addressId: shippingAddresses[0]._id,
				orderNumber: '000001',
				subTotal: 1000,
				discount: 0,
				tax: 0,
				deliveryFee: 10,
				total: 1010,
				deliveryInstruction: 'Please deliver ASAP',
				paymentMethod: 'COD',
				status: 'Ordered',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[0]._id,
				paymentId: shippingAddresses[1]._id,
				addressId: shippingAddresses[1]._id,
				orderNumber: '000002',
				subTotal: 2000,
				discount: 0,
				tax: 0,
				deliveryFee: 20,
				total: 2020,
				deliveryInstruction: 'Please deliver ASAP',
				paymentMethod: 'COD',
				status: 'Ordered',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[0]._id,
				paymentId: shippingAddresses[1]._id,
				addressId: shippingAddresses[1]._id,
				orderNumber: '000003',
				subTotal: 3000,
				discount: 0,
				tax: 0,
				deliveryFee: 30,
				total: 3030,
				deliveryInstruction: 'Please deliver ASAP',
				paymentMethod: 'COD',
				status: 'Ordered',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[1]._id,
				paymentId: shippingAddresses[1]._id,
				addressId: shippingAddresses[0]._id,
				orderNumber: '000004',
				subTotal: 4000,
				discount: 0,
				tax: 0,
				deliveryFee: 40,
				total: 4040,
				deliveryInstruction: 'Please deliver ASAP',
				paymentMethod: 'COD',
				status: 'Ordered',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				userId: users[1]._id,
				paymentId: shippingAddresses[1]._id,
				addressId: shippingAddresses[1]._id,
				orderNumber: '000005',
				subTotal: 5000,
				discount: 0,
				tax: 0,
				deliveryFee: 50,
				total: 5050,
				deliveryInstruction: 'Please deliver ASAP',
				paymentMethod: 'COD',
				status: 'Ordered',
				createdAt: new Date()
			}
		];

		await seeder(Order, orders, 'Order');
	} catch (error) {
		console.log('Enable to get company or shipping addresses or users in order seeder:', error);
	}
};

export default orderSeeder;