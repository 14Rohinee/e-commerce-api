import Brand from '../models/brandModel.js';
import seeder from './seeder.js';
import Company from '../models/companyModel.js';

const brandSeeder = async () => {
	try {
		const company = await Company.findOne();
		const brands = [
			{
				companyId: company._id,
				name: 'Sony',
				slug: 'sony',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Samsung',
				slug: 'samsung',
				status: 'active',
				createdAt: new Date()
			},
			{
				companyId: company._id,
				name: 'Apple',
				slug: 'apple',
				status: 'active',
				createdAt: new Date()
			}
		];
	
		await seeder(Brand, brands, 'Brand');
	} catch (error) {
		console.log('Enable to get company in brand seeder:', error);
	}	
};

export default brandSeeder;