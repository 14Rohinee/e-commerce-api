import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const pushNotificationSettingSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	onesignalAppId: { type: String, required: true },
	onesignalApiKey: { type: String, required: true },
	notificationLogo: { type: String, required: false },
	status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false }
}
);

pushNotificationSettingSchema.plugin(uniqueValidator);

export default pushNotificationSettingSchema;
