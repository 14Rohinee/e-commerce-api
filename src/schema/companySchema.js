import { Schema } from 'mongoose';

const userSchema = new Schema({
	name: { type: String, required: true },
	email: { type: String, required: true },
	phone: { type: String, required: true },
	website: { type: String, required: true },
	dateFormat: { type: String, required: false },
	timeFormat: { type: String, required: false },
	timeZone: { type: String, required: false },
	language: { type: String, required: false },
	appName: { type: String, required: false },
	logo: { type: String, required: false },
	status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }

});

export default userSchema;
