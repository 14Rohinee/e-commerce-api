import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const cartSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	userId: { type: Schema.Types.ObjectId, required: true },
	couponId: { type: Schema.Types.ObjectId, required: false },
	orderId: { type: Schema.Types.ObjectId, required: false },
	addressId: { type: Schema.Types.ObjectId, required: true },
	subTotal: { type: Number, required: true },
	discount: { type: Number, default: 0 },
	tax: { type: Number, default: 0 },
	deliveryFee: { type: Number, default: 0 },
	total: { type: Number, required: true },
	deliveryInstruction: { type: String, required: false },
	paymentMethod: { type: String, enum: ['Stripe', 'Paypal', 'Razorpay', 'COD'], default: 'COD' },
	status: { type: String, enum: ['Ordered', 'Canceled', 'Delivered'], default: 'Ordered' },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }

});

cartSchema.plugin(uniqueValidator);

export default cartSchema;