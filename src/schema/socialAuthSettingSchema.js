import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const socialAuthSettingSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	facebookStatus: {
		type: String,
		enum: ['active', 'inactive'],
		default: 'inactive',
	},
	facebookClientId: { type: String, required: false },
	facebookClientSecret: { type: String, required: false },
	googleClientId: { type: String, required: false },
	googleClientSecret: { type: String, required: false },
	googleStatus: {
		type: String,
		enum: ['active', 'inactive'],
		default: 'inactive',
	},
	twitterClientId: { type: String, required: false },
	twitterSecretId: { type: String, required: false },
	twitterStatus: {
		type: String,
		enum: ['active', 'inactive'],
		default: 'inactive',
	},
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
});

socialAuthSettingSchema.plugin(uniqueValidator);

export default socialAuthSettingSchema;
