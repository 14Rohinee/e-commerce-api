import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const orderItemSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	orderId: { type: Schema.Types.ObjectId, required: true },
	productId: { type: Schema.Types.ObjectId, required: true },
	unitPrice: { type: Number, required: true },
	quantity: { type: Number, required: true },
	totalPrice: { type: Number, required: true },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }
});


orderItemSchema.plugin(uniqueValidator);

export default orderItemSchema;
