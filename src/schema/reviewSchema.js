import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const reviewSchema = new Schema({
	userId: { type: Schema.Types.ObjectId, required: true },
	productId: { type: Schema.Types.ObjectId, required: true }, // Payment method
	rating: { type: Number, min: 1, default: 1 },
	review: { type: String, required: true },
	status: { type: String, enum: ['active', 'inactive'], default: 'active' },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }
});


reviewSchema.plugin(uniqueValidator);

export default reviewSchema;
