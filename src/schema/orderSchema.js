import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const orderSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	userId: { type: Schema.Types.ObjectId, required: true },
	couponId: { type: Schema.Types.ObjectId, required: false },
	paymentId: { type: Schema.Types.ObjectId, required: true },
	addressId: { type: Schema.Types.ObjectId, required: true },
	orderNumber: { type: String, required: false },
	subTotal: { type: Number, required: true },
	discount: { type: Number, default: 0 },
	tax: { type: Number, default: 0 },
	deliveryFee: { type: Number, default: 0 },
	total: { type: Number, required: true },
	deliveryInstruction: { type: String, required: false },
	status: { type: String, enum: ['Ordered', 'Canceled', 'Delivered'], default: 'Ordered' },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }
});


orderSchema.plugin(uniqueValidator);

export default orderSchema;
