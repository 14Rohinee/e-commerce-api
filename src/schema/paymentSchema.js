import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const paymentSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: true },
	orderId: { type: Schema.Types.ObjectId, required: true },
	paymentMethod: { type: String, required: true }, // Payment method
	status: { type: String, enum: [null, 'Incompleted', 'Completed', 'Processing', 'Failed'], default: null },
	amount: { type: Number, default: 0 },
	paymentGatewayResponse: { type: Number, default: null },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now }
});


paymentSchema.plugin(uniqueValidator);

export default paymentSchema;
