import { Schema } from 'mongoose';

const userSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: false },
	employeeId: { type: String, required: false },
	name: { type: String, required: true },
	email: { type: String, required: true, unique: true },
	password: { type: String, required: true, minlength: 6 },
	mobile: { type: String, required: true, minlength: 10, unique: true },
	dob: { type: String, required: false },
	gender: { type: String, enum: ['male', 'female', 'other', null], default: null },
	image: { type: String, required: false },
	role: { type: String, enum: ['customer', 'admin', 'employee', 'superadmin'], default: 'client' },
	status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
	verifiedAt: { type: Date, required: false },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false },
	lastLogin: { type: Date, required: false }
});


export default userSchema;
