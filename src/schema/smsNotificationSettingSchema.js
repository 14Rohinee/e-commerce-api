import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const smsNotificationSettingSchema = new Schema({
	companyId: { type: Schema.Types.ObjectId, required: false },
	smsSid: { type: String, required: true },
	smsAuthToken: { type: String, required: true },
	smsFromNumber: { type: String, required: true },
	status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false }
});

smsNotificationSettingSchema.plugin(uniqueValidator);

export default smsNotificationSettingSchema;
