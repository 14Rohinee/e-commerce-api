import { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const subCategorySchema = new Schema({
	categoryId: { type: Schema.Types.ObjectId, required: true },
	slug: { type: String, required: true, unique: true },
	name: { type: String, required: true, unique: true },
	status: { type: String, enum: ['active', 'inactive'], default: 'inactive' },
	createdBy: { type: Schema.Types.ObjectId, required: false },
	createdAt: { type: Date, default: Date.now },
	updatedBy: { type: Schema.Types.ObjectId, required: false },
	updatedAt: { type: Date, required: false },
	deletedBy: { type: Schema.Types.ObjectId, required: false }, // Soft delete
	deletedAt: { type: Date, required: false }
});

subCategorySchema.plugin(uniqueValidator);

export default subCategorySchema;
