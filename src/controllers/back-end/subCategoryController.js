import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import subCategoryService from '../../services/db-services/subCategoryService.js';

const index = async (req, res, next) => {
	try {
		const subCategories = await subCategoryService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			subCategories
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const subCategory = await subCategoryService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'SubCategory Created Successfully',
			subCategory,
		);
		
		res.json(response);

	} catch (err) {
		console.error('Error while creating Category:', err);
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const subCategory = await subCategoryService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'SubCategory fetched Successfully',
			subCategory,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const subCategory = await subCategoryService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'SubCategory Updated Successfully',
			subCategory,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const subCategory = await subCategoryService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'SubCategory Deleted Successfully',
			subCategory,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };