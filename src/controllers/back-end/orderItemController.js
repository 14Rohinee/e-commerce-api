import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import orderItemService from '../../services/db-services/orderItemService.js';

const index = async (req, res, next) => {
	try {
		const orderItems = await orderItemService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			orderItems
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const orderItem = await orderItemService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Order Item Created Successfully',
			orderItem,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const orderItem = await orderItemService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Item Fetched Successfully',
			orderItem,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const orderItem = await orderItemService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Item Updated Successfully',
			orderItem,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const orderItem = await orderItemService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Item Deleted Successfully',
			orderItem,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };