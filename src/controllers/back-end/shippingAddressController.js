import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import shippingAddressService from '../../services/db-services/shippingAddressService.js';

const index = async (req, res, next) => {
	try {
		const shippingAddresses = await shippingAddressService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			shippingAddresses
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const shippingAddress = await shippingAddressService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Shipping Address Created Successfully',
			shippingAddress,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const shippingAddress = await shippingAddressService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Shipping Address Fetched Successfully',
			shippingAddress,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const shippingAddress = await shippingAddressService.update(req.params.id, req.body);

		const response = HttpSuccess(
			'Success',
			200,
			'Shipping Address Updated Successfully',
			shippingAddress,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const shippingAddress = await shippingAddressService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Shipping Address Deleted Successfully',
			shippingAddress,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };