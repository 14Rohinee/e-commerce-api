import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import pushNotificationSettingService from '../../services/db-services/pushNotificationSettingService.js';

const index = async (req, res, next) => {
	try {
		const pushNotifications = await pushNotificationSettingService.get();
		const response = HttpSuccess(
			'Success',
			200,
			'',
			pushNotifications
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const pushNotification = await pushNotificationSettingService.update(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Push Notification Setting Updated Successfully',
			pushNotification,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, update };