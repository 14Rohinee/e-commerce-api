import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import userService from '../../services/db-services/userService.js';
import Excel from 'exceljs';
import User from '../../models/userModel.js';

const index = async (req, res, next) => {
	try {
		const users = await userService.get(req.params.page, req.params.limit);

		if (users) {
			const response = HttpSuccess(
				'Success',
				200,
				'',
				users
			);

			res.json(response);
		} else {
			const error = new HttpError('User not found', 404);
			return next(error);
		}

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		// upload(req, res, (err) => {
		// 	if (err) {
		// 		console.error('Error while uploading image:', err);
		// 		const error = new HttpError('Error while uploading image', 500);
		// 		return next(error);
		// 	} else if(req.file === undefined) {
		// 		console.log('No file selected.');
		// 	}
		// });

		const user = await userService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'User Created Successfully',
			user,
		);
		
		res.json(response);

	} catch (err) {
		console.error('Error while creating user:', err);
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const user = await userService.find(req.params.id);
		res.json(user);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const user = await userService.update(req.params.id, req.body);

		if (user) {
			const response = HttpSuccess(
				'Success',
				200,
				'User Updated Successfully',
				user,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('User not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const user = await userService.destroy(req.params.id);

		if (user) {
			const response = HttpSuccess(
				'Success',
				200,
				'User Deleted Successfully',
				user,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('User not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const exportUser = async (req, res, next) => {
	try {
		const workBook = new Excel.Workbook();
		const workSheet = workBook.addWorksheet('Sheet1');

		workSheet.columns = [
			{ header: 'Name', key: 'name', width: 20 },
			{ header: 'Email', key: 'email', width: 30 },
			{ header: 'Gender', key: 'gender', width: 10 },
			{ header: 'Mobile', key: 'mobile', width: 30 },
			{ header: 'Date of birth', key: 'dob', width: 10 },
			{ header: 'Status', key: 'status', width: 10 }
		];

		const projection = { name: 1, email: 1, mobile: 1, gender: 1, status: 1, _id: 0 };
		const users = await User.find({}, projection);

		users.forEach((user) => {
			workSheet.addRow(user);
		});

		await workBook.xlsx.writeFile('users.xlsx');

		const response = HttpSuccess(
			'Success',
			200,
			'User Excel file written successfully',
			users
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};


// const bulkAction = async (req, res, next) => {
// 	try {
// 		const user = await userService.bulkAction(req.params.id);

// 		if (user) {
// 			const response = HttpSuccess(
// 				'Success',
// 				200,
// 				'User Deleted Successfully',
// 				user,
// 			);
			
// 			res.json(response);
// 		} else {
// 			const error = new HttpError('User not found', 404);
// 			return next(error);
// 		}
// 	} catch (err) {
// 		const error = new HttpError(err, 500);
// 		return next(error);
// 	}
// };


export default { index, store, edit, update, destroy, exportUser };
