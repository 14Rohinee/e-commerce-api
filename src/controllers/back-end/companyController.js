import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import companyService from '../../services/db-services/companyService.js';

const index = async (req, res, next) => {
	try {
		const companies = await companyService.get(req.params.page, req.params.limit);

		if (companies) {
			const response = HttpSuccess(
				'Success',
				200,
				'',
				companies
			);

			res.json(response);
		} else {
			const error = new HttpError('Company not found', 404);
			return next(error);
		}

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const company = await companyService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Company Created Successfully',
			company,
		);
		
		res.json(response);

	} catch (err) {
		console.error('Error while creating company:', err);
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	
	try {
		const company = await companyService.find(req.params.id);
		res.json(company);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const company = await companyService.update(req.params.id, req.body);

		if (company) {
			const response = HttpSuccess(
				'Success',
				200,
				'Company Updated Successfully',
				company,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Company not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const company = await companyService.destroy(req.params.id);

		if (company) {
			const response = HttpSuccess(
				'Success',
				200,
				'Company Deleted Successfully',
				company,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Company not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };
