import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import reviewService from '../../services/db-services/reviewService.js';

const index = async (req, res, next) => {
	try {
		const reviews = await reviewService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			reviews
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const review = await reviewService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Review Created Successfully',
			review,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const review = await reviewService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Review Fetched Successfully',
			review,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const review = await reviewService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Review Updated Successfully',
			review,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update };