import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import couponService from '../../services/db-services/couponService.js';

const index = async (req, res, next) => {
	try {
		const coupons = await couponService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			coupons
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const coupon = await couponService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Coupon Created Successfully',
			coupon,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const coupon = await couponService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Coupon Fetched Successfully',
			coupon,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const coupon = await couponService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Coupon Updated Successfully',
			coupon,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const coupon = await couponService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Coupon Deleted Successfully',
			coupon,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };