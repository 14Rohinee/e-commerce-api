import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import smsNotificationSettingService from '../../services/db-services/smsNotificationSettingService.js';

const index = async (req, res, next) => {
	try {
		const smsNotifications = await smsNotificationSettingService.get();
		const response = HttpSuccess(
			'Success',
			200,
			'',
			smsNotifications
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const smsNotification = await smsNotificationSettingService.update(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Sms Notification Setting Updated Successfully',
			smsNotification,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, update };