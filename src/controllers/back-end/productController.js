import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import productService from '../../services/db-services/productService.js';

const index = async (req, res, next) => {
	try {
		const products = await productService.get(req.params.page, req.params.limit);

		if (products) {
			const response = HttpSuccess(
				'Success',
				200,
				'',
				products
			);

			res.json(response);
		} else {
			const error = new HttpError('Product not found', 404);
			return next(error);
		}

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const product = await productService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Product Created Successfully',
			product,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const product = await productService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Product Fetched Successfully',
			product,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const product = await productService.update(req.params.id, req.body);

		if (product) {
			const response = HttpSuccess(
				'Success',
				200,
				'Product Updated Successfully',
				product,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Product not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const product = await productService.destroy(req.params.id);

		if (product) {
			const response = HttpSuccess(
				'Success',
				200,
				'Product Deleted Successfully',
				product,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Product not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };