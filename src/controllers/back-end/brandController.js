import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import brandService from '../../services/db-services/brandService.js';

const index = async (req, res, next) => {
	try {
		const brands = await brandService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			brands
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const brand = await brandService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Brand Created Successfully',
			brand,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const brand = await brandService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Brand Fetched Successfully',
			brand,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const brand = await brandService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Brand Updated Successfully',
			brand,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const brand = await brandService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Brand Deleted Successfully',
			brand,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };