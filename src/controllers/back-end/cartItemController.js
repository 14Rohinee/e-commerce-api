import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import cartItemService from '../../services/db-services/cartItemService.js';

const index = async (req, res, next) => {
	try {
		const cartItems = await cartItemService.get(req.params.page, req.params.limit);

		if (cartItems) {
			const response = HttpSuccess(
				'Success',
				200,
				'',
				cartItems
			);

			res.json(response);
		} else {
			const error = new HttpError('Cart Item not found', 404);
			return next(error);
		}

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const cartItem = await cartItemService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Item Created Successfully',
			cartItem,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const cartItem = await cartItemService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Item Fetched Successfully',
			cartItem,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const cartItem = await cartItemService.update(req.params.id, req.body);

		if (cartItem) {
			const response = HttpSuccess(
				'Success',
				200,
				'Cart Item Updated Successfully',
				cartItem,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Cart Item not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const cartItem = await cartItemService.destroy(req.params.id);

		if (cartItem) {
			const response = HttpSuccess(
				'Success',
				200,
				'Cart Item Deleted Successfully',
				cartItem,
			);
			
			res.json(response);
		} else {
			const error = new HttpError('Cart Item not found', 404);
			return next(error);
		}
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };