import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import categoryService from '../../services/db-services/categoryService.js';

const index = async (req, res, next) => {
	try {
		const categories = await categoryService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			categories
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const category = await categoryService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Category Created Successfully',
			category,
		);
		
		res.json(response);

	} catch (err) {
		console.error('Error while creating Category:', err);
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const category = await categoryService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Category fetched Successfully',
			category,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const category = await categoryService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Category Updated Successfully',
			category,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const category = await categoryService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Category Deleted Successfully',
			category,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };