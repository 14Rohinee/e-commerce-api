import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import cartService from '../../services/db-services/cartService.js';

const index = async (req, res, next) => {
	try {
		const carts = await cartService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			carts
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const cart = await cartService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Created Successfully',
			cart,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const cart = await cartService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Fetched Successfully',
			cart,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const cart = await cartService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Updated Successfully',
			cart,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const cart = await cartService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Cart Deleted Successfully',
			cart,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };