import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import socialAuthSettingService from '../../services/db-services/socialAuthSettingService.js';

const index = async (req, res, next) => {
	try {
		const socialAuthSettings = await socialAuthSettingService.get();
		const response = HttpSuccess(
			'Success',
			200,
			'',
			socialAuthSettings
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const socialAuthSetting = await socialAuthSettingService.update(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Social Auth Setting Updated Successfully',
			socialAuthSetting,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, update };