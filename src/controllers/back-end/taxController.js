import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import taxService from '../../services/db-services/taxService.js';

const index = async (req, res, next) => {
	try {
		const taxes = await taxService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			taxes
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const tax = await taxService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Tax Created Successfully',
			tax,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const tax = await taxService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Tax Fetched Successfully',
			tax,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const tax = await taxService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Tax Updated Successfully',
			tax,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const tax = await taxService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Tax Deleted Successfully',
			tax,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };