import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import languageService from '../../services/db-services/languageService.js';

const index = async (req, res, next) => {
	try {
		const languages = await languageService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			languages
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const language = await languageService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Language Created Successfully',
			language,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const language = await languageService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Language Fetched Successfully',
			language,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const language = await languageService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Language Updated Successfully',
			language,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const language = await languageService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Language Deleted Successfully',
			language,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };