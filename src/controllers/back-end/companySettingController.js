import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import companySettingService from '../../services/db-services/companySettingService.js';

const index = async (req, res, next) => {
	try {
		const companySettings = await companySettingService.get();
		const response = HttpSuccess(
			'Success',
			200,
			'',
			companySettings
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const companySetting = await companySettingService.update(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Company Setting Updated Successfully',
			companySetting,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, update };