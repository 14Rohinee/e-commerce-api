import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import emailSettingService from '../../services/db-services/emailSettingService.js';

const index = async (req, res, next) => {
	try {
		const emailSettings = await emailSettingService.get();
		const response = HttpSuccess(
			'Success',
			200,
			'',
			emailSettings
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const emailSetting = await emailSettingService.update(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Email Setting Updated Successfully',
			emailSetting,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, update };