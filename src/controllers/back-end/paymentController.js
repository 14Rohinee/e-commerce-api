import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import paymentService from '../../services/db-services/paymentService.js';

const index = async (req, res, next) => {
	try {
		const payments = await paymentService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			payments
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const payment = await paymentService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Payment Created Successfully',
			payment,
		);
		
		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store };