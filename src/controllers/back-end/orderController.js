import HttpError from '../../services/httpErrorService.js';
import HttpSuccess from '../../services/HttpSuccessService.js';
import orderService from '../../services/db-services/orderService.js';

const index = async (req, res, next) => {
	try {
		const orders = await orderService.get(req.params.page, req.params.limit);
		const response = HttpSuccess(
			'Success',
			200,
			'',
			orders
		);

		res.json(response);

	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const store = async (req, res, next) => {
	try {
		const order = await orderService.store(req.body);                         
		const response = HttpSuccess(
			'Success',
			200,
			'Order Created Successfully',
			order,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const edit = async (req, res, next) => {
	try {
		const order = await orderService.edit(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Fetched Successfully',
			order,
		);

		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const update = async (req, res, next) => {
	try {
		const order = await orderService.update(req.params.id, req.body);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Updated Successfully',
			order,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

const destroy = async (req, res, next) => {
	try {
		const order = await orderService.destroy(req.params.id);
		const response = HttpSuccess(
			'Success',
			200,
			'Order Deleted Successfully',
			order,
		);
		
		res.json(response);
	} catch (err) {
		const error = new HttpError(err, 500);
		return next(error);
	}
};

export default { index, store, edit, update, destroy };