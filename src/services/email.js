/* eslint-disable no-undef */
/* eslint-disable no-mixed-spaces-and-tabs */
import nodemailer from 'nodemailer';
import pug from 'pug';
import { htmlToText } from 'html-to-text';
import path from 'path';

const __dirname = path.resolve();

class Email {
	constructor(user) {
		this.to = user.email;
		this.name = user.name;
		this.from = `Rohinee Tandekar <${ process.env.EMAIL_FROM }>`;
	}

	newTransport () {
		if (process.env.NODE_ENV === 'production') {
			return 1;
		}

		return nodemailer.createTransport({
			host: process.env.EMAIL_HOST,
			port: process.env.EMAIL_PORT,
			auth: {
				user: process.env.EMAIL_USERNAME,
				pass: process.env.EMAIL_PASSWORD
			}
		});
	}

	async send(template, subject) {
		const baseDirectory = path.join(__dirname, '..');
		const filePath = path.resolve(baseDirectory, 'e-commerce/src/services/views/emails', `${template}.pug`);
		const html = pug.renderFile(filePath, {
			name: this.name,
			subject
		});

		const mailOptions = {
			from: this.from,
			to: this.to,
			subject,
			html,
			text: htmlToText(html)
		};

		// create a transport and send email
		await this.newTransport().sendMail(mailOptions);
	}

	async sendWelcome() {
		await this.send('Welcome', `Welcome ${this.name}`);
	}
}

export default Email;