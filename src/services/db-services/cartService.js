import Cart from '../../models/cartModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.cartsCount = await Cart.countDocuments();
		if (endIndex < result.cartsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.carts = await Cart.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (cartData) => {
	try {
		const cart = await new Cart(cartData);
		await cart.save();

		return cart;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Cart ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const cart = await Cart.findById(id);

		if (!cart) {
			throw new HttpError('Cart not found', 404);
		}

		return cart;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, cartData) => {
	const updateFields = {};

	Object.keys(cartData).forEach(key => {
		updateFields[key] = cartData[key];
	});

	try {
		const cart = await Cart.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!cart) {
			throw new HttpError('Error while finding the Cart', 404);
		}

		return cart;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const cart = await Cart.findOneAndDelete({ _id: id });

		if (!cart) {
			throw new HttpError('Cart not found', 404);
		}

		return cart;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
