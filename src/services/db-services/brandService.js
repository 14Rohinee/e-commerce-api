import Brand from '../../models/brandModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.brandsCount = await Brand.countDocuments();
		if (endIndex < result.brandsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.brands = await Brand.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (brandData) => {
	try {
		const brand = await new Brand(brandData);
		await brand.save();

		return brand;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid brand ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const brand = await Brand.findById(id);

		if (!brand) {
			throw new HttpError('Brand not found', 404);
		}

		return brand;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, brandData) => {
	const updateFields = {};

	Object.keys(brandData).forEach(key => {
		updateFields[key] = brandData[key];
	});

	try {
		const brand = await Brand.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!brand) {
			throw new HttpError('Error while finding the brand', 404);
		}

		return brand;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const brand = await Brand.findOneAndDelete({ _id: id });

		if (!brand) {
			throw new HttpError('Brand not found', 404);
		}

		return brand;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
