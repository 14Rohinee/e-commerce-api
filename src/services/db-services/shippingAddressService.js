import ShippingAddress from '../../models/shippingAddressModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.shippingAddressesCount = await ShippingAddress.countDocuments();
		if (endIndex < result.shippingAddressesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.shippingAddressesCount = await ShippingAddress.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (shippingAddressData) => {
	try {
		const shippingAddress = await new ShippingAddress(shippingAddressData);
		await shippingAddress.save();

		return shippingAddress;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Shipping Address ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const shippingAddress = await ShippingAddress.findById(id);

		if (!shippingAddress) {
			throw new HttpError('Shipping Address not found', 404);
		}

		return shippingAddress;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, shippingAddressData) => {
	const updateFields = {};

	Object.keys(shippingAddressData).forEach(key => {
		updateFields[key] = shippingAddressData[key];
	});

	try {
		const shippingAddress = await ShippingAddress.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!shippingAddress) {
			throw new HttpError('Error while finding the Shipping Address', 404);
		}

		return shippingAddress;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const shippingAddress = await ShippingAddress.findOneAndDelete({ _id: id });

		if (!shippingAddress) {
			throw new HttpError('Shipping Address not found', 404);
		}

		return shippingAddress;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
