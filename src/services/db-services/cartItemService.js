import CartItem from '../../models/cartItemModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.cartItemsCount = await CartItem.countDocuments();
		if (endIndex < result.cartItemsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.cartItems = await CartItem.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (cartItemData) => {
	try {
		const cartItem = await new CartItem(cartItemData);
		await cartItem.save();

		return cartItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Cart Item ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const cartItem = await CartItem.findById(id);

		if (!cartItem) {
			throw new HttpError('Cart Item not found', 404);
		}

		return cartItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, cartItemData) => {
	const updateFields = {};

	Object.keys(cartItemData).forEach(key => {
		updateFields[key] = cartItemData[key];
	});

	try {
		const cartItem = await CartItem.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!cartItem) {
			throw new HttpError('Error while finding the Cart Item', 404);
		}

		return cartItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const cartItem = await CartItem.findOneAndDelete({ _id: id });

		if (!cartItem) {
			throw new HttpError('Cart Item not found', 404);
		}

		return cartItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
