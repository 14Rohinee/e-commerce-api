import User from '../../models/userModel.js';
import HttpError from '../httpErrorService.js';
import Email from '../email.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.usersCount = await User.countDocuments();
		if (endIndex < result.usersCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.users = await User.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (userData) => {
	try {
		const user = await new User(userData);
		await user.save();
		await new Email(user).sendWelcome();

		return user;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const find = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid user ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const user = await User.findById(id);

		if (!user) {
			throw new HttpError('User not found', 404);
		}

		return user;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, userData) => {
	const updateFields = {};

	Object.keys(userData).forEach(key => {
		updateFields[key] = userData[key];
	});

	try {
		const user = await User.findByIdAndUpdate(id,{ $set: updateFields }, { new: true });

		if (!user) {
			throw new HttpError('Error while finding or updating the user', 404);
		}

		return user;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const user = await User.findOneAndDelete({ _id: id });

		if (!user) {
			throw new HttpError('User not found', 404);
		}

		return user;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

// const bulkAction = async (id) => {
// 	try {
		
// 		const user = await User.findOneAndDelete({ _id: id });

// 		if (!user) {
// 			throw new HttpError('User not found', 404);
// 		}

// 		return user;
// 	} catch (err) {
// 		throw new HttpError(err.message, 500);
// 	}
// };

export default { get, store, find, update, destroy };
