import SubCategory from '../../models/subCategoryModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.subCategoriesCount = await SubCategory.countDocuments();
		if (endIndex < result.subCategoriesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.subCategories = await SubCategory.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (subCategoryData) => {
	try {
		const subCategory = await new SubCategory(subCategoryData);
		await subCategory.save();

		return subCategory;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid SubCategory ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const subCategory = await SubCategory.findById(id);

		if (!subCategory) {
			throw new HttpError('SubCategory not found', 404);
		}

		return subCategory;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, subCategoryData) => {
	const updateFields = {};

	Object.keys(subCategoryData).forEach(key => {
		updateFields[key] = subCategoryData[key];
	});

	try {
		const subCategory = await SubCategory.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!subCategory) {
			throw new HttpError('Error while finding the SubCategory', 404);
		}

		return subCategory;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const subCategory = await SubCategory.findOneAndDelete({ _id: id });

		if (!subCategory) {
			throw new HttpError('SubCategory not found', 404);
		}

		return subCategory;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
