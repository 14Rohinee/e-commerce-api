import CompanySetting from '../../models/companySettingModel.js';
import HttpError from '../httpErrorService.js';

const get = async () => {

	try {
		const companySetting = await CompanySetting.findOne();

		if (!companySetting) {
			throw new HttpError('Company Setting not found', 404);
		}

		return companySetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (companySettingData) => {
	try {
		const companySetting = await CompanySetting.updateOne(companySettingData);

		if (!companySetting) {
			throw new HttpError('Error while updating Company Setting', 500);
		}

		return companySetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, update };
