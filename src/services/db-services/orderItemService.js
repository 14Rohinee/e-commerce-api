import OrderItem from '../../models/orderItemModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.orderItemsCount = await OrderItem.countDocuments();
		if (endIndex < result.orderItemsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.orderItems = await OrderItem.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (orderItemData) => {
	try {
		const orderItem = await new OrderItem(orderItemData);
		await orderItem.save();

		return orderItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Order Item ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const orderItem = await OrderItem.findById(id);

		if (!orderItem) {
			throw new HttpError('Order Item not found', 404);
		}

		return orderItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, orderItemData) => {
	const updateFields = {};

	Object.keys(orderItemData).forEach(key => {
		updateFields[key] = orderItemData[key];
	});

	try {
		const orderItem = await OrderItem.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!orderItem) {
			throw new HttpError('Error while finding the Order Item', 404);
		}

		return orderItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const orderItem = await OrderItem.findOneAndDelete({ _id: id });

		if (!orderItem) {
			throw new HttpError('Order Item not found', 404);
		}

		return orderItem;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
