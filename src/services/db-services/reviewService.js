import Review from '../../models/reviewModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.reviewsCount = await Review.countDocuments();
		if (endIndex < result.reviewsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.reviews = await Review.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (reviewData) => {
	try {
		const review = await new Review(reviewData);
		await review.save();

		return review;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Review ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const review = await Review.findById(id);

		if (!review) {
			throw new HttpError('Review not found', 404);
		}

		return review;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, reviewData) => {
	const updateFields = {};

	Object.keys(reviewData).forEach(key => {
		updateFields[key] = reviewData[key];
	});

	try {
		const review = await Review.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!review) {
			throw new HttpError('Error while finding the Review', 404);
		}

		return review;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update };
