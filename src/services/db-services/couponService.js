import Coupon from '../../models/couponModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.couponsCount = await Coupon.countDocuments();
		if (endIndex < result.couponsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.coupons = await Coupon.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (couponData) => {
	try {
		const coupon = await new Coupon(couponData);
		await coupon.save();

		return coupon;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Coupon ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const coupon = await Coupon.findById(id);

		if (!coupon) {
			throw new HttpError('Coupon not found', 404);
		}

		return coupon;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, couponData) => {
	const updateFields = {};

	Object.keys(couponData).forEach(key => {
		updateFields[key] = couponData[key];
	});

	try {
		const coupon = await Coupon.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!coupon) {
			throw new HttpError('Error while finding the Coupon', 404);
		}

		return coupon;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const coupon = await Coupon.findOneAndDelete({ _id: id });

		if (!coupon) {
			throw new HttpError('Coupon not found', 404);
		}

		return coupon;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
