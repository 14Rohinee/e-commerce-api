import Payment from '../../models/paymentModel.js';
import HttpError from '../httpErrorService.js';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.paymentsCount = await Payment.countDocuments();
		if (endIndex < result.paymentsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.payment = await Payment.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (paymentData) => {
	try {
		const payment = await new Payment(paymentData);
		await payment.save();

		return payment;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store };
