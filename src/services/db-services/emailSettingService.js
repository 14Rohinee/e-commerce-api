import EmailSetting from '../../models/emailSettingModel.js';
import HttpError from '../httpErrorService.js';

const get = async () => {

	try {
		const emailSetting = await EmailSetting.findOne();

		if (!emailSetting) {
			throw new HttpError('Email Setting not found', 404);
		}

		return emailSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (emailSettingData) => {
	try {
		const emailSetting = await EmailSetting.updateOne(emailSettingData);

		if (!emailSetting) {
			throw new HttpError('Error while updating Email Setting', 500);
		}

		return emailSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, update };
