import Order from '../../models/orderModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.ordersCount = await Order.countDocuments();
		if (endIndex < result.ordersCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.orders = await Order.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (orderData) => {
	try {
		const order = await new Order(orderData);
		await order.save();

		return order;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Order ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const order = await Order.findById(id);

		if (!order) {
			throw new HttpError('Order not found', 404);
		}

		return order;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, orderData) => {
	const updateFields = {};

	Object.keys(orderData).forEach(key => {
		updateFields[key] = orderData[key];
	});

	try {
		const order = await Order.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!order) {
			throw new HttpError('Error while finding the Order', 404);
		}

		return order;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const order = await Order.findOneAndDelete({ _id: id });

		if (!order) {
			throw new HttpError('Order not found', 404);
		}

		return order;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
