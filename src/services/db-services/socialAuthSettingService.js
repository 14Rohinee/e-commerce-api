import SocialAuthSetting from '../../models/socialAuthSettingModel.js';
import HttpError from '../httpErrorService.js';

const get = async () => {

	try {
		const socialAuthSetting = await SocialAuthSetting.findOne();

		if (!socialAuthSetting) {
			throw new HttpError('Social Auth Setting not found', 404);
		}

		return socialAuthSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (socialAuthSettingData) => {
	try {
		const socialAuthSetting = await SocialAuthSetting.updateOne(socialAuthSettingData);

		if (!socialAuthSetting) {
			throw new HttpError('Error while updating Social Auth Setting', 500);
		}

		return socialAuthSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, update };
