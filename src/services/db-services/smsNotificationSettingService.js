import SmsNotificationSetting from '../../models/smsNotificationSettingModel.js';
import HttpError from '../httpErrorService.js';

const get = async () => {

	try {
		const smsNotificationSetting = await SmsNotificationSetting.findOne();

		if (!smsNotificationSetting) {
			throw new HttpError('Sms Notification Setting not found', 404);
		}

		return smsNotificationSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (smsNotificationSettingData) => {
	try {
		const smsNotificationSetting = await SmsNotificationSetting.updateOne(smsNotificationSettingData);

		if (!smsNotificationSetting) {
			throw new HttpError('Error while updating sms Notification Setting', 500);
		}

		return smsNotificationSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, update };
