import Language from '../../models/languageModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.languagesCount = await Language.countDocuments();
		if (endIndex < result.languagesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.languages = await Language.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (languageData) => {
	try {
		const language = await new Language(languageData);
		await language.save();

		return language;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Language ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const language = await Language.findById(id);

		if (!language) {
			throw new HttpError('Language not found', 404);
		}

		return language;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, languageData) => {
	const updateFields = {};

	Object.keys(languageData).forEach(key => {
		updateFields[key] = languageData[key];
	});

	try {
		const language = await Language.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!language) {
			throw new HttpError('Error while finding the Language', 404);
		}

		return language;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const language = await Language.findOneAndDelete({ _id: id });

		if (!language) {
			throw new HttpError('Language not found', 404);
		}

		return language;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
