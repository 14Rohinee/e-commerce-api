import Tax from '../../models/taxModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.taxesCount = await Tax.countDocuments();
		if (endIndex < result.taxesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.taxes = await Tax.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (taxData) => {
	try {
		const tax = await new Tax(taxData);
		await tax.save();

		return tax;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Tax ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const tax = await Tax.findById(id);

		if (!tax) {
			throw new HttpError('Tax not found', 404);
		}

		return tax;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, taxData) => {
	const updateFields = {};

	Object.keys(taxData).forEach(key => {
		updateFields[key] = taxData[key];
	});

	try {
		const tax = await Tax.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!tax) {
			throw new HttpError('Error while finding the Tax', 404);
		}

		return tax;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const tax = await Tax.findOneAndDelete({ _id: id });

		if (!tax) {
			throw new HttpError('Tax not found', 404);
		}

		return tax;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
