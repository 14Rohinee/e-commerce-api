import PushNotificationSetting from '../../models/pushNotificationSettingModel.js';
import HttpError from '../httpErrorService.js';

const get = async () => {

	try {
		const pushNotificationSetting = await PushNotificationSetting.findOne();

		if (!pushNotificationSetting) {
			throw new HttpError('Email Setting not found', 404);
		}

		return pushNotificationSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (pushNotificationSettingData) => {
	try {
		const pushNotificationSetting = await PushNotificationSetting.updateOne(pushNotificationSettingData);

		if (!pushNotificationSetting) {
			throw new HttpError('Error while updating Push Notification Setting', 500);
		}

		return pushNotificationSetting;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, update };
