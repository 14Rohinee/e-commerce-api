import Company from '../../models/companyModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.companiesCount = await Company.countDocuments();
		if (endIndex < result.companiesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.companies = await Company.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (companyData) => {
	try {
		const company = await new Company(companyData);
		await company.save();

		return company;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const find = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid company ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const company = await Company.findById(id);

		if (!company) {
			throw new HttpError('Company not found', 404);
		}

		return company;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, companyData) => {
	const updateFields = {};

	Object.keys(companyData).forEach(key => {
		updateFields[key] = companyData[key];
	});

	try {
		const company = await Company.findByIdAndUpdate(id,{ $set: updateFields }, { new: true });

		if (!company) {
			throw new HttpError('Error while finding or updating the company', 404);
		}

		return company;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const company = await Company.findOneAndDelete({ _id: id });

		if (!company) {
			throw new HttpError('Company not found', 404);
		}

		return company;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, find, update, destroy };