import Product from '../../models/productModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.productsCount = await Product.countDocuments();
		if (endIndex < result.productsCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.products = await Product.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (productData) => {
	try {
		const product = await new Product(productData);
		await product.save();

		return product;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid Product ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const product = await Product.findById(id);

		if (!product) {
			throw new HttpError('Product not found', 404);
		}

		return product;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, productData) => {
	const updateFields = {};

	Object.keys(productData).forEach(key => {
		updateFields[key] = productData[key];
	});

	try {
		const product = await Product.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!product) {
			throw new HttpError('Error while finding the Product', 404);
		}

		return product;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const product = await Product.findOneAndDelete({ _id: id });

		if (!product) {
			throw new HttpError('Product not found', 404);
		}

		return product;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
