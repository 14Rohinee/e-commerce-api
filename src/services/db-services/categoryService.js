import Category from '../../models/categoryModel.js';
import HttpError from '../httpErrorService.js';
import mongoose from 'mongoose';

const get = async (page, limit) => {
	page = parseInt(page) || 1;
	limit = parseInt(limit) || 10;
	const startIndex = (page - 1) * limit;
	const endIndex = page * limit;

	const result = {};

	try {
		result.categoriesCount = await Category.countDocuments();
		if (endIndex < result.categoriesCount) {
			result.next = {
				page: page + 1,
				limit
			};
		}

		if (startIndex > 0) {
			result.previous = {
				page: page - 1,
				limit
			};
		}

		result.current = { page, limit };
		result.categories = await Category.find().limit(limit).skip(startIndex);

		return result;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const store = async (categoryData) => {
	try {
		const category = await new Category(categoryData);
		await category.save();

		return category;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const edit = async (id) => {
	try {
		if (!mongoose.Types.ObjectId.isValid(id)) {
			throw new HttpError('Invalid category ID format', 400);
		}

		id = new mongoose.Types.ObjectId(id);
		const category = await Category.findById(id);

		if (!category) {
			throw new HttpError('Category not found', 404);
		}

		return category;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const update = async (id, categoryData) => {
	const updateFields = {};

	Object.keys(categoryData).forEach(key => {
		updateFields[key] = categoryData[key];
	});

	try {
		const category = await Category.findOneAndUpdate({ _id: id }, updateFields, { new: true });
		
		if (!category) {
			throw new HttpError('Error while finding the category', 404);
		}

		return category;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

const destroy = async (id) => {
	try {
		
		const category = await Category.findOneAndDelete({ _id: id });

		if (!category) {
			throw new HttpError('Category not found', 404);
		}

		return category;
	} catch (err) {
		throw new HttpError(err.message, 500);
	}
};

export default { get, store, edit, update, destroy };
