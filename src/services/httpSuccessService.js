const HttpSuccess = (status, code, message, data) => {
	return { status, code, message, data };
};

export default HttpSuccess;