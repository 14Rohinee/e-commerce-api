import express from 'express';
import categoryController from '../../controllers/back-end/categoryController.js';
const router = express.Router();

router.get('/edit/:id', categoryController.edit);
router.get('/:page?/:limit?', categoryController.index);
router.post('/store', categoryController.store);
router.patch('/update/:id', categoryController.update);
router.delete('/delete/:id', categoryController.destroy);

export default router;