import express from 'express';
import cartController from '../../controllers/back-end/cartController.js';
const router = express.Router();

router.get('/edit/:id', cartController.edit);
router.get('/:page?/:limit?', cartController.index);
router.post('/store', cartController.store);
router.patch('/update/:id', cartController.update);
router.delete('/delete/:id', cartController.destroy);

export default router;