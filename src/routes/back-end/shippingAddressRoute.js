import express from 'express';
import shippingAddressController from '../../controllers/back-end/shippingAddressController.js';
const router = express.Router();

router.get('/edit/:id', shippingAddressController.edit);
router.get('/:page?/:limit?', shippingAddressController.index);
router.post('/store', shippingAddressController.store);
router.patch('/update/:id', shippingAddressController.update);
router.delete('/delete/:id', shippingAddressController.destroy);

export default router;