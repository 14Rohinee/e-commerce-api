import express from 'express';
import taxController from '../../controllers/back-end/taxController.js';
const router = express.Router();

router.get('/edit/:id', taxController.edit);
router.get('/:page?/:limit?', taxController.index);
router.post('/store', taxController.store);
router.patch('/update/:id', taxController.update);
router.delete('/delete/:id', taxController.destroy);

export default router;