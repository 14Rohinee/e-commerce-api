import express from 'express';
import paymentController from '../../controllers/back-end/paymentController.js';
const router = express.Router();

router.get('/:page?/:limit?', paymentController.index);
router.post('/store', paymentController.store);

export default router;