import express from 'express';
import userController from '../../controllers/back-end/userController.js';
const router = express.Router();

router.get('/edit/:id', userController.edit);
router.get('/:page?/:limit?', userController.index);
router.post('/store', userController.store);
router.patch('/update/:id', userController.update);
router.delete('/delete/:id', userController.destroy);
router.post('/export', userController.exportUser);

export default router;