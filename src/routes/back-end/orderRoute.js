import express from 'express';
import orderController from '../../controllers/back-end/orderController.js';
const router = express.Router();

router.get('/edit/:id', orderController.edit);
router.get('/:page?/:limit?', orderController.index);
router.post('/store', orderController.store);
router.patch('/update/:id', orderController.update);
router.delete('/delete/:id', orderController.destroy);

export default router;