import express from 'express';
import couponController from '../../controllers/back-end/couponController.js';
const router = express.Router();

router.get('/edit/:id', couponController.edit);
router.get('/:page?/:limit?', couponController.index);
router.post('/store', couponController.store);
router.patch('/update/:id', couponController.update);
router.delete('/delete/:id', couponController.destroy);

export default router;