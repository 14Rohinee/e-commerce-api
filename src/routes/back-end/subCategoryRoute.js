import express from 'express';
import subCategoryController from '../../controllers/back-end/subCategoryController.js';
const router = express.Router();

router.get('/edit/:id', subCategoryController.edit);
router.get('/:page?/:limit?', subCategoryController.index);
router.post('/store', subCategoryController.store);
router.patch('/update/:id', subCategoryController.update);
router.delete('/delete/:id', subCategoryController.destroy);

export default router;