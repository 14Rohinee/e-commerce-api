import express from 'express';
import pushNotificationSettingController from '../../controllers/back-end/pushNotificationSettingController.js';
const router = express.Router();

router.get('/', pushNotificationSettingController.index);
router.patch('/update', pushNotificationSettingController.update);

export default router;