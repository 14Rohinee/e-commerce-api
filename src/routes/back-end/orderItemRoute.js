import express from 'express';
import orderItemController from '../../controllers/back-end/orderItemController.js';
const router = express.Router();

router.get('/edit/:id', orderItemController.edit);
router.get('/:page?/:limit?', orderItemController.index);
router.post('/store', orderItemController.store);
router.patch('/update/:id', orderItemController.update);
router.delete('/delete/:id', orderItemController.destroy);

export default router;