import express from 'express';
import brandController from '../../controllers/back-end/brandController.js';
const router = express.Router();

router.get('/edit/:id', brandController.edit);
router.get('/:page?/:limit?', brandController.index);
router.post('/store', brandController.store);
router.patch('/update/:id', brandController.update);
router.delete('/delete/:id', brandController.destroy);

export default router;