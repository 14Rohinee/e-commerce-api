import express from 'express';
import smsNotificationSettingController from '../../controllers/back-end/smsNotificationSettingController.js';
const router = express.Router();

router.get('/', smsNotificationSettingController.index);
router.patch('/update', smsNotificationSettingController.update);

export default router;