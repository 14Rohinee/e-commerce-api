import express from 'express';
import productController from '../../controllers/back-end/productController.js';
const router = express.Router();

router.get('/edit/:id', productController.edit);
router.get('/:page?/:limit?', productController.index);
router.post('/store', productController.store);
router.patch('/update/:id', productController.update);
router.delete('/delete/:id', productController.destroy);

export default router;