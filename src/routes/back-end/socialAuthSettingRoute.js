import express from 'express';
import socialAuthSettingController from '../../controllers/back-end/socialAuthSettingController.js';
const router = express.Router();

router.get('/', socialAuthSettingController.index);
router.patch('/update', socialAuthSettingController.update);

export default router;