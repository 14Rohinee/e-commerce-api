import express from 'express';
import companySettingController from '../../controllers/back-end/companySettingController.js';
const router = express.Router();

router.get('/', companySettingController.index);
router.patch('/update', companySettingController.update);

export default router;