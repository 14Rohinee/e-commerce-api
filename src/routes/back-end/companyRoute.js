import express from 'express';
import companyController from '../../controllers/back-end/companyController.js';
const router = express.Router();

router.get('/:id/edit', companyController.edit);
router.get('/:page?/:limit?', companyController.index);
router.post('/store', companyController.store);
router.patch('/update/:id', companyController.update);
router.delete('/delete/:id', companyController.destroy);

export default router;