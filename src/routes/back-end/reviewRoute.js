import express from 'express';
import reviewController from '../../controllers/back-end/reviewController.js';
const router = express.Router();

router.get('/edit/:id', reviewController.edit);
router.get('/:page?/:limit?', reviewController.index);
router.post('/store', reviewController.store);
router.patch('/update/:id', reviewController.update);

export default router;