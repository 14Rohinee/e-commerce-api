import express from 'express';
import cartItemController from '../../controllers/back-end/cartItemController.js';
const router = express.Router();

router.get('/edit/:id', cartItemController.edit);
router.get('/:page?/:limit?', cartItemController.index);
router.post('/store', cartItemController.store);
router.patch('/update/:id', cartItemController.update);
router.delete('/delete/:id', cartItemController.destroy);

export default router;