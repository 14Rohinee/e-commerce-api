import express from 'express';
import languageController from '../../controllers/back-end/languageController.js';
const router = express.Router();

router.get('/edit/:id', languageController.edit);
router.get('/:page?/:limit?', languageController.index);
router.post('/store', languageController.store);
router.patch('/update/:id', languageController.update);
router.delete('/delete/:id', languageController.destroy);

export default router;