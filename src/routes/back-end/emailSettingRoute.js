import express from 'express';
import emailSettingController from '../../controllers/back-end/emailSettingController.js';
const router = express.Router();

router.get('/', emailSettingController.index);
router.patch('/update', emailSettingController.update);

export default router;