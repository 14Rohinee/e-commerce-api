import mongoose from 'mongoose';
import paymentSchema from '../schema/paymentSchema.js';

export default mongoose.model('Payment', paymentSchema);