import mongoose from 'mongoose';
import subCategorySchema from '../schema/subCategorySchema.js';

export default mongoose.model('SubCategory', subCategorySchema);