import mongoose from 'mongoose';
import brandSchema from '../schema/brandSchema.js';

export default mongoose.model('Brand', brandSchema);