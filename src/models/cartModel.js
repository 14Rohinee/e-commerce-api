import mongoose from 'mongoose';
import cartSchema from '../schema/cartSchema.js';

export default mongoose.model('Cart', cartSchema);