import mongoose from 'mongoose';
import shippingAddressSchema from '../schema/shippingAddressSchema.js';

export default mongoose.model('ShippingAddress', shippingAddressSchema);