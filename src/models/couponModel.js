import mongoose from 'mongoose';
import couponSchema from '../schema/couponSchema.js';

export default mongoose.model('Coupon', couponSchema);