import mongoose from 'mongoose';
import companySchema from '../schema/companySchema.js';

export default mongoose.model('Company', companySchema);