import mongoose from 'mongoose';
import companySettingSchema from '../schema/companySettingSchema.js';

export default mongoose.model('CompanySetting', companySettingSchema);