import mongoose from 'mongoose';
import orderSchema from '../schema/orderSchema.js';

export default mongoose.model('Order', orderSchema);