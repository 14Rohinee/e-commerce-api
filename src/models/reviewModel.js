import mongoose from 'mongoose';
import reviewSchema from '../schema/reviewSchema.js';

export default mongoose.model('Review', reviewSchema);