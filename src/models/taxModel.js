import mongoose from 'mongoose';
import taxSchema from '../schema/taxSchema.js';

export default mongoose.model('Tax', taxSchema);