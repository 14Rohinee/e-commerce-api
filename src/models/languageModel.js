import mongoose from 'mongoose';
import languageSchema from '../schema/languageSchema.js';

export default mongoose.model('Language', languageSchema);