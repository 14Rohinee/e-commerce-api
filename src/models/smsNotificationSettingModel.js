import mongoose from 'mongoose';
import smsNotificationSettingSchema from '../schema/smsNotificationSettingSchema.js';

export default mongoose.model('SmsNotificationSetting', smsNotificationSettingSchema);