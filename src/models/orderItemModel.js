import mongoose from 'mongoose';
import orderItemSchema from '../schema/orderItemSchema.js';

export default mongoose.model('OrderItem', orderItemSchema);