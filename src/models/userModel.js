import mongoose from 'mongoose';
import userSchema from '../schema/userSchema.js';
import bcrypt from 'bcrypt';

userSchema.pre('save', async function (next) {
	if (!this.isModified('password')) {
		return next();
	}

	this.password = await bcrypt.hash(this.password, 10);
	next();
});

export default mongoose.model('User', userSchema);