import mongoose from 'mongoose';
import pushNotificationSettingSchema from '../schema/pushNotificationSettingSchema.js';

export default mongoose.model('PushNotificationSetting', pushNotificationSettingSchema);