import mongoose from 'mongoose';
import emailSettingSchema from '../schema/emailSettingSchema.js';

export default mongoose.model('EmailSetting', emailSettingSchema);