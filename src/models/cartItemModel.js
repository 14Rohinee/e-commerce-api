import mongoose from 'mongoose';
import cartItemSchema from '../schema/cartItemSchema.js';

export default mongoose.model('CartItem', cartItemSchema);