import mongoose from 'mongoose';
import socialAuthSettingSchema from '../schema/socialAuthSettingSchema.js';

export default mongoose.model('SocialAuthSetting', socialAuthSettingSchema);