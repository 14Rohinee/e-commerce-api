import mongoose from 'mongoose';
import productSchema from '../schema/productSchema.js';

export default mongoose.model('Product', productSchema);